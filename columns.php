<?php

namespace TmColumns;
use TinyMceButtons;

if(!defined('TMCOLUMNS_URL'))
	define('TMCOLUMNS_URL', get_stylesheet_directory_uri().'/vendor/mupic/wp-columns/');
if(!defined('TMCOLUMNS_PATH'))
	define('TMCOLUMNS_PATH', get_stylesheet_directory().'/vendor/mupic/wp-columns/');

if(WP_DEBUG == 'true' && !defined('TMCOLUMNS_DEBUG'))
	define('TMCOLUMNS_DEBUG', true);
if(!defined('TMCOLUMNS_DEBUG'))
	define('TMCOLUMNS_DEBUG', false);

if(!defined('TMCOLUMNS_INCLUDE_GRID'))
	define('TMCOLUMNS_INCLUDE_GRID', true);
if(!defined('TMCOLUMNS_INCLUDE_ADMIN_GRID'))
	define('TMCOLUMNS_INCLUDE_ADMIN_GRID', true);


class TinyMCEColumns{
	function init(){
		if(TMCOLUMNS_INCLUDE_GRID)
			add_action( 'wp_enqueue_scripts', array($this, 'wp_scripts') );
		add_action( 'admin_enqueue_scripts', array($this, 'admin_scripts') );

		$this->add_button();
	}

	function add_button(){
		$min = TMCOLUMNS_DEBUG? '' : '.min';
		$AddCustomButtonTinymce = new TinyMceButtons\AddButton();
		$AddCustomButtonTinymce->row = 2;
		$AddCustomButtonTinymce->add('bootstrap_columns', TMCOLUMNS_URL."js/columns-admin{$min}.js", -2);
		$AddCustomButtonTinymce->register_style(TMCOLUMNS_URL."css/columns-tinymce.css");
		if(TMCOLUMNS_INCLUDE_ADMIN_GRID)
			$AddCustomButtonTinymce->register_style(TMCOLUMNS_URL."css/grid.css");
		$AddCustomButtonTinymce->init();
	}

	function wp_scripts(){
		wp_enqueue_style( 'bootstrap-grid', TMCOLUMNS_URL."css/grid.css", array(), '4.0.0' );
	}

	function admin_scripts(){

		$templates = array(
			'column' => [
				'title' => ['Column', 'localize'],
				'content' => '[row class="" width=""]'.
								'[col media="" class=""]{::insert_content::}[/col]'.
							'[/row]'
			],
		);

		$templates = apply_filters('tm_columns_templates', $templates);

		if($templates){
			$new_templates = array();
			foreach ($templates as $key => $template) {
				if(empty($template['title']))
					continue;
				$title = is_array($template['title'])? __($template['title'][0], $template['title'][1]) : __($template['title']);
				$new_templates[$key]['text'] = $title;
				$new_templates[$key]['value'] = $template['content'];
			}
			wp_localize_script('jquery', 'TMColumns_template', array(
				'template' => $new_templates,
			) );
		}
	}
}
add_action('init', array(new TinyMCEColumns, 'init'));


function shortcode_row( $atts , $content = null ) {

	extract( shortcode_atts(
		array(
			'width' => false,
			'height' => false,
			'class' => false,
			'style' => false,
			'id' => false
		), $atts )
	);

	if(stripos('%', $width) !== false)
		$width += 'px';

	$attrs = theme_attr(array(
		'id' => $id,
		'class' => 'row wp-columns-row '.$class,
		'style' => ($width? 'width:'.$width.'; ' : '').($height? 'height:'.$height.'px; ' : '').$style,
	));

	$result = "<div {$attrs}>";
		$result .= force_balance_tags(do_shortcode( $content ));

	return $result;

}

function shortcode_row_close( $content ){
	$array = array (
		'[/row]' => '</div>'
	);
	return strtr( $content, $array ); 
}
add_filter( 'the_content', '\TmColumns\shortcode_row_close' );
add_filter( 'widget_text', '\TmColumns\shortcode_row_close' );

function shortcode_col( $atts , $content = null ) {

	extract( shortcode_atts(
		array(
			'height' => false,
			'media' => false,
			'size' => false,
			'class' => false,
			'style' => false,
			'id' => false
		), $atts )
	);
	$attrs = theme_attr(array(
		'id' => $id,
		'class' => implode(' ', array_diff(array($class, 'col', 'wp-columns-col', $size, $media), array(''))),
		'style' => ($height? 'height:'.$height.'px; ' : '').$style,
	));

	$result = "<div {$attrs}>";
		$result .= force_balance_tags(do_shortcode( $content ));

	return $result;

}
function shortcode_col_close( $content ){
	$array = array (
		'[/col]' => '</div>'
	);
	return strtr( $content, $array ); 
}
add_filter( 'the_content', '\TmColumns\shortcode_col_close' );
add_filter( 'widget_text', '\TmColumns\shortcode_col_close' );

function columns_helper( $content ){
	$array = array (
		'<p>[row' => '[row', 
		'row]</p>' => 'row]', 
		'<br />[row' => '[row',
		'row]<br />' => 'row]',
		'<p>[col' => '[col', 
		'col]</p>' => 'col]', 
		'<br />[col' => '[col',
		'col]<br />' => 'col]',
	);

	return strtr( $content, $array ); 
}
add_filter( 'the_content', '\TmColumns\columns_helper', 1 );
add_filter( 'widget_text', '\TmColumns\columns_helper', 1 );

add_shortcode( 'row', '\TmColumns\shortcode_row' );
add_shortcode( 'col', '\TmColumns\shortcode_col' );