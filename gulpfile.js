const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-csso');
const uglifyJS = require('gulp-uglify');
const rename = require("gulp-rename");
const pump = require('pump');

gulp.task('build-scss', function() {
	gulp.src('./css/columns-tinymce.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('columns-tinymce.css'))
		.pipe(gulp.dest('./css/'));

	gulp.src('./css/grid.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('grid.css'))
		.pipe(gulp.dest('./css/'));
});

gulp.task('build-js', function() {
	return gulp.src(['./js/columns-admin.js'])
		.pipe(uglifyJS())
		.pipe(rename('columns-admin.min.js'))
		.pipe(gulp.dest('./js/'));
});

gulp.task('js-debug', function(cb) {
	pump([
		gulp.src(['./js/columns-admin.js']),
		uglifyJS(),
		gulp.dest('./js/')
	], cb);
});

gulp.task('watch', function() {
	gulp.watch(['./js/columns-admin.js'], ['build-js']);
	gulp.watch(['./css/columns-tinymce.scss'], ['build-scss']);
});
gulp.task('default', ['build-scss', 'build-js']);