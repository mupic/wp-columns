/* global getUserSetting, setUserSetting */
( function( tinymce ) {
// Set the minimum value for the modals z-index higher than #wpadminbar (100000)
if ( tinymce.ui.FloatPanel.zIndex < 100100 ) {
	tinymce.ui.FloatPanel.zIndex = 100100;
}

tinymce.PluginManager.add( 'bootstrap_columns', function( editor ) {
	var wpAdvButton, style,
		DOM = tinymce.DOM,
		each = tinymce.each,
		__ = editor.editorManager.i18n.translate,
		$ = window.jQuery,
		wp = window.wp,
		hasWpautop = ( wp && wp.editor && wp.editor.autop && editor.getParam( 'wpautop', true ) );


	if ( $ ) {
		$( document ).triggerHandler( 'tinymce-editor-setup', [ editor ] );
	}

	var ColMedia = {
		sm: 'if window ≥ 576px',
		md: 'if window ≥ 768px',
		lg: 'if window ≥ 992px',
		xl: 'if window ≥ 1200px',
	};
	var ColSize = 12;

	var flexDirection = {
		fullName: 'flex-direction',
		class: 'flex',
		array: [
			{
				text: 'row',
				value: 'row',
			},
			{
				text: 'row-reverse',
				value: 'row-reverse',
			},
			{
				text: 'column',
				value: 'column',
			},
			{
				text: 'column-reverse',
				value: 'column-reverse',
			},
		],
	};
	var flexWrap = {
		fullName: 'flex-wrap',
		class: 'flex',
		array: [
			{
				text: 'nowrap',
				value: 'nowrap',
			},
			{
				text: 'wrap',
				value: 'wrap',
			},
			{
				text: 'wrap-reverse',
				value: 'wrap-reverse',
			},
		],
	};
	var justifyContent = {
		fullName: 'justify-content',
		class: 'justify-content',
		array: [
			{
				text: 'flex-start',
				value: 'start',
			},
			{
				text: 'flex-end',
				value: 'end',
			},
			{
				text: 'center',
				value: 'center',
			},
			{
				text: 'space-between',
				value: 'between',
			},
			{
				text: 'space-around',
				value: 'around',
			},
		],
	};
	var alignItems = {
		fullName: 'align-items',
		class: 'align-items',
		array: [
			{
				text: 'stretch',
				value: 'stretch',
			},
			{
				text: 'flex-start',
				value: 'start',
			},
			{
				text: 'flex-end',
				value: 'end',
			},
			{
				text: 'center',
				value: 'center',
			},
			{
				text: 'baseline',
				value: 'baseline',
			},
		],
	};
	var alignSelf = {
		fullName: 'align-self',
		class: 'align-self',
		array: alignItems['array']
	}
	var alignContent = {
		fullName: 'align-content',
		class: 'align-content',
		array: [
			{
				text: 'stretch',
				value: 'stretch',
			},
			{
				text: 'flex-start',
				value: 'start',
			},
			{
				text: 'flex-end',
				value: 'end',
			},
			{
				text: 'center',
				value: 'center',
			},
			{
				text: 'space-between',
				value: 'between',
			},
			{
				text: 'space-around',
				value: 'around',
			},
		],
	};

	var addMenuSizes = function(NameOption, prefix, multiOption){
		prefix = prefix? prefix+'-' : '';
		var menu = [], count = 1;
		var Regexp = new RegExp('( +)?(col-'+prefix+'\\d+)( +)?', 'gi');

		for (var i = ColSize; i >= 1; i--) {
			menu.push(
				{
					text: Math.round(100 / ColSize * count) + '% (' + count + '/' +ColSize + ')',
					value: 'col-' + prefix + count,
					onclick: function() {
						var $this = this;
						var td = getCurrentTd();
						if(multiOption)
							var currentOption = td.attr('data-save-'+NameOption).replace(Regexp, '');

						if($this.state.data.active){
							if(multiOption){
								td.attr('data-save-'+NameOption, currentOption);
							}else{
								td.attr('data-save-'+NameOption, '');
							}
							$this.active(false);
						}else{
							$this.active(true);
							if(multiOption){
								td.attr('data-save-'+NameOption, (currentOption? currentOption + ' ' : '') + $this.state.data.value);
							}else{
								td.attr('data-save-'+NameOption, $this.state.data.value);
							}
							td.addClass($this.state.data.value);
							$.each($this._parent._items, function(index, el) {
								if($this._id != el._id){
									el.active(false);
									td.removeClass(el.state.data.value);
								}
							});
						}

						editor.undoManager.add();
					},
					onpostrender: function() {
						enableIfSelected(this, 'td');
						var btn = this;

						$(this.$el).hover(function(){
							var td = getCurrentTd();
							if(!td) return false;
							if(!multiOption){
								var option = td.attr('data-save-'+NameOption);
								td.removeClass(option);
							}
							td[0].className = td[0].className.replace(Regexp, ' ');
							td.addClass(btn.state.data.value);
						}).parent('div')
						.hover(function(){}, function(){
							var td = getCurrentTd();
							if(!td) return false;
							var option = td.attr('data-save-'+NameOption)
							td.removeClass(btn.state.data.value);
							td.addClass(option);
						});

						var activeCurrentMenu = function(){
							var td = getCurrentTd();
							if(!td) return false;
							var option = td.attr('data-save-'+NameOption);
							if($.inArray(btn.state.data.value, option.split(' ')) > -1){
								btn.active(true);
							}else{
								btn.active(false);
							}

						};
						activeCurrentMenu();
						editor.on('NodeChange', function(e) {
							activeCurrentMenu();
						});
					}
				}
			);

			count++;
		}

		return menu;
	}

	var clearClasses = function(classes){
		var removeClasses = ['row', 'col', 'wp-b-row-tag', 'mce-wp-b-row', 'wp-b-col-tag', 'mce-wp-b-col', 'wp-columns-row', 'wp-columns-col'];
		var ClassRegExp = '(';
		$.each(removeClasses, function(index, reClass) {
			ClassRegExp += '\\b'+reClass+'\\b|';
		});
		ClassRegExp = ClassRegExp.substr(0, ClassRegExp.length-1);
		ClassRegExp += ')';
		var Regexp = '(';
		$.each([flexDirection, flexWrap, justifyContent, alignItems, alignSelf, alignContent], function(index, value) {
			$.each(value['array'], function(arrIndex, arrValue) {
				Regexp += '\\b'+value['class']+'-'+arrValue.value+'\\b|';
			});
		});
		Regexp = Regexp.substr(0, Regexp.length-1);
		Regexp += ')';

		Regexp = Regexp.replace(/-/g, '_____');
		ClassRegExp = ClassRegExp.replace(/-/g, '_____');
		classes = classes.replace(/-/g, '_____');
		classes = classes.replace(new RegExp(Regexp, 'gi'), '').replace(/(\bcol_____[\w]{0,})/gi, ' ').replace(new RegExp(ClassRegExp, 'gi'), ' ').replace(/\s{2,}/gi, ' ').replace(/^\s{1,}/gi, '').replace(/\s{1,}$/gi, '');
		classes = classes.replace(/_____/g, '-');

		return classes;
	}

	var removeAttrOptions = function(option, attrName){
		if(!this.length) return '';
		var attrOption = typeof this[0] == 'string'? this.toString().split(' ') : this.attr(attrName).split(' ');
		var newOption = [];
		$.each(attrOption, function(index, value) {
			if(value != option)
				newOption.push(value);
		});

		if(typeof this[0] != 'string')
			this.attr(attrName, newOption);

		return newOption.join(' ');
	}

	$.fn.removeAttrOptions = removeAttrOptions;
	String.prototype.removeAttrOptions = removeAttrOptions;

	var parentsIsRowTable = function(td){
		if(!td.parent('tr').parent('tbody').parent('table[data-wp-b-row="row"]').length)
			return false;

		return true;
	}

	var getCurrentTd = function(searchParentTag){
		var selected = $(editor.selection.getSel().focusNode);
		//editor.selection.getNode() - multiselect
		var td = selected.is('td')? selected : $(selected).closest('td');

		if(!parentsIsRowTable(td))
			return false;

		if(searchParentTag)
			td = td.closest(searchParentTag);

		if(!td.length) return false;

		return td;
	}

	var enableIfSelected = function(btn, tag){
		if(!getCurrentTd())
			btn.disabled(1);
		editor.on('NodeChange', function(e) {
			var element = $(e.element);
			var td = element.is('td')? element : element.closest('td');
			if(!parentsIsRowTable(td)){
				btn.disabled(1);
				return;
			}

			btn.disabled(!element.closest(tag).length);
		});
	}

	var clearCol = function(element){
		element.find('*:not(strong):not(em):not(b):not(i):not(span):not(font):not(h1):not(h2):not(h3):not(h4):not(h5):not(h6):not(p):not(div):not(td)').remove();
		$(element.find('strong,em,b,i,span,font,h1,h2,h3,h4,h5,h6,p,div,td').get().reverse()).each(function(index, el) {
			var $this = $(this);
			var children = $this.children('*').clone(true);
			$this.empty().append(children);
		});
		element.find('*:empty').append('<br data-mce-bogus="1">');

		return element;
	}

	var getWidth = function(element){
		var width = element[0].style.width || element.attr('width') || '';
		if(width.indexOf('%') < 0){
			width = width? parseInt(width) : width;
		}
		return width;
	}
	var getHeight = function(element){
		var height = element[0].style.height || element.attr('height') || '';
		return parseInt(height) || '';
	}

	var myUnshift = function(array, addValue){
		var newArray = JSON.parse(JSON.stringify(array));
		newArray.unshift(addValue);
		return newArray;
	}

	var getLinkWebRef = function(style){
		return '<a href="https://webref.ru/css/'+style+'" style=" float: right; font-size: 10px; margin-top: -0.6em;" rel="noopener noreferrer" target="_blank">Documentation</a>';
	};

	editor.addButton( 'bootstrap_columns', function(){
		var menu = [];
		$.each(TMColumns_template.template, function(index, value) {
			menu.push({
				text: value.text,
				onclick: function() {
					editor.insertContent(value.value.replace('{::insert_content::}', editor.selection.getContent()));
				},
			});
		});
		return {
			text: 'Columns',
			type: 'menubutton',
			menu: [
				{
					text: 'Templates',
					menu: menu,
				},
				{
					text: 'Options',
					onclick: function() {
						var table = getCurrentTd('table');
						var tbody = table.children('tbody');
						var classes = tbody.attr('class').split(' ');
						var cleanClasses = clearClasses(classes.join(' '));
						var width = getWidth(table);
						var height = getHeight(table);

						editor.windowManager.open({
							title: 'Options',
							body: [
								{
									type: 'textbox',
									name: 'class',
									label: 'Class',
									value: cleanClasses,
								},
								{
									type: 'textbox',
									name: 'width',
									label: 'Width',
									value: width,
								},
								{
									type: 'textbox',
									name: 'height',
									label: 'Height',
									value: height,
								},
								{
									type: 'listbox',
									name: 'flexDirection',
									label: flexDirection['fullName'],
									values: myUnshift(flexDirection['array'], {text: '----', value:''}),
									onPostRender: function () {
										var $this = this;
										$.each(flexDirection['array'], function(index, value) {
											if($.inArray(flexDirection['class'] +'-'+ value.value, classes) > -1){
												$this.value(value.value);
											}
										});
									}
								},
								{
									type: 'container',
									html: getLinkWebRef(flexDirection['fullName']),
								},
								{
									type: 'listbox',
									name: 'flexWrap',
									label: flexWrap['fullName'],
									values: myUnshift(flexWrap['array'], {text: '----', value:''}),
									onPostRender: function () {
										var $this = this;
										$.each(flexWrap['array'], function(index, value) {
											if($.inArray(flexWrap['class'] +'-'+ value.value, classes) > -1){
												$this.value(value.value);
											}
										});
									}
								},
								{
									type: 'container',
									html: getLinkWebRef(flexWrap['fullName']),
								},
								{
									type: 'listbox',
									name: 'justifyContent',
									label: justifyContent['fullName'],
									values: myUnshift(justifyContent['array'], {text: '----', value:''}),
									onPostRender: function () {
										var $this = this;
										$.each(justifyContent['array'], function(index, value) {
											if($.inArray(justifyContent['class'] +'-'+ value.value, classes) > -1){
												$this.value(value.value);
											}
										});
									}
								},
								{
									type: 'container',
									html: getLinkWebRef(justifyContent['fullName']),
								},
								{
									type: 'listbox',
									name: 'alignItems',
									label: alignItems['fullName'],
									values: myUnshift(alignItems['array'], {text: '----', value:''}),
									onPostRender: function () {
										var $this = this;
										$.each(alignItems['array'], function(index, value) {
											if($.inArray(alignItems['class'] +'-'+ value.value, classes) > -1){
												$this.value(value.value);
											}
										});
									}
								},
								{
									type: 'container',
									html: getLinkWebRef(alignItems['fullName']),
								},
								{
									type: 'listbox',
									name: 'alignContent',
									label: alignContent['fullName'],
									values: myUnshift(alignContent['array'], {text: '----', value:''}),
									onPostRender: function () {
										var $this = this;
										$.each(alignContent['array'], function(index, value) {
											if($.inArray(alignContent['class'] +'-'+ value.value, classes) > -1){
												$this.value(value.value);
											}
										});
									}
								},
								{
									type: 'container',
									html: getLinkWebRef(alignContent['fullName']),
								},
							],
							onsubmit: function(e) {
								table.css({'width': '', 'height': ''}).attr('width', e.data.width).attr('height', e.data.height);

								tbody.removeClass(cleanClasses);
								if(e.data.class)
									tbody.addClass(e.data.class);

								$.each(flexDirection['array'], function(index, value){
									tbody.removeClass(flexDirection['class'] +'-'+ value.value);
								});
								$.each(flexWrap['array'], function(index, value){
									tbody.removeClass(flexWrap['class'] +'-'+ value.value);
								});
								$.each(justifyContent['array'], function(index, value){
									tbody.removeClass(justifyContent['class'] +'-'+ value.value);
								});
								$.each(alignItems['array'], function(index, value){
									tbody.removeClass(alignItems['class'] +'-'+ value.value);
								});
								$.each(alignContent['array'], function(index, value){
									tbody.removeClass(alignContent['class'] +'-'+ value.value);
								});
								if(e.data.flexDirection) tbody.addClass(flexDirection['class'] +'-'+ e.data.flexDirection);
								if(e.data.flexWrap) tbody.addClass(flexWrap['class'] +'-'+ e.data.flexWrap);
								if(e.data.justifyContent) tbody.addClass(justifyContent['class'] +'-'+ e.data.justifyContent);
								if(e.data.alignItems) tbody.addClass(alignItems['class'] +'-'+ e.data.alignItems);
								if(e.data.alignContent) tbody.addClass(alignContent['class'] +'-'+ e.data.alignContent);

								editor.undoManager.add();
							}
						});

					},
					onpostrender: function() {
						enableIfSelected(this, 'table');
					}
				},
				{
					separator: 'before',
					text: 'Row',
					menu: [
						{
							text: 'Insert row before',
							onclick: function() {
								var tr = getCurrentTd('tr');
								if(!tr) return false;
								var cloneTr = tr.clone(true);
								tr.before(clearCol(cloneTr));

								editor.undoManager.add();
							},
							onpostrender: function() {
								enableIfSelected(this, 'tr');
							}
						},
						{
							text: 'Insert row after',
							onclick: function() {
								var tr = getCurrentTd('tr');
								if(!tr) return false;
								var cloneTr = tr.clone(true);
								tr.after(clearCol(cloneTr));

								editor.undoManager.add();
							},
							onpostrender: function() {
								enableIfSelected(this, 'tr');
							}
						},
						{
							text: 'Remove',
							onclick: function() {
								var tr = getCurrentTd('tr');
								if(!tr) return false;
								var table = tr.closest('table');
								tr.remove();
								if(!$('tr', table).length)
									table.remove();

								editor.undoManager.add();
							},
							onpostrender: function() {
								enableIfSelected(this, 'tr');
							}
						},
					],
				},
				{
					text: 'Cell',
					menu: [
						{
							text: 'Options',
							onclick: function() {
								var td = getCurrentTd('td');
								var classes = td.attr('class').split(' ');
								var cleanClasses = clearClasses(classes.join(' '));
								var height = getHeight(td);

								editor.windowManager.open({
									title: 'Cell Options',
									body: [
										{
											type: 'textbox',
											name: 'class',
											label: 'Class',
											value: cleanClasses,
										},
										{
											type: 'textbox',
											name: 'height',
											label: 'Height',
											value: height,
										},
										{
											type: 'listbox',
											name: 'alignSelf',
											label: alignSelf['fullName'],
											values: myUnshift(alignSelf['array'], {text: '----', value:''}),
											onPostRender: function() {
												var $this = this;
												$.each(alignSelf['array'], function(index, value) {
													if($.inArray(alignSelf['class'] +'-'+ value.value, classes) > -1){
														$this.value(value.value);
													}
												});
											}
										},
										{
											type: 'container',
											html: getLinkWebRef(alignSelf['fullName']),
										},
									],
									onsubmit: function(e) {
										var CurrentClass = td.attr('data-save-class');

										td.removeClass(cleanClasses);
										$.each(cleanClasses.split(' '), function(index, valClass) {
											CurrentClass = String(CurrentClass).removeAttrOptions(valClass);
										});
										if(e.data.class){
											td.addClass(e.data.class);
											CurrentClass = (CurrentClass? CurrentClass + ' ' : '') + e.data.class;
										}

										td.css('height', '').attr('height', e.data.height);
										$.each(alignSelf['array'], function(index, value){
											td.removeClass(alignSelf['class'] +'-'+ value.value);
											CurrentClass = String(CurrentClass).removeAttrOptions(alignSelf['class'] +'-'+ value.value);
										});
										if(e.data.alignSelf){
											td.addClass(alignSelf['class'] +'-'+ e.data.alignSelf);
											CurrentClass = (CurrentClass? CurrentClass + ' ' : '') + alignSelf['class'] +'-'+ e.data.alignSelf;
										}

										td.attr('data-save-class', CurrentClass);

										editor.undoManager.add();
									}
								});

							},
							onpostrender: function() {
								enableIfSelected(this, 'td');
							}
						},
						{
							text: 'Media size',
							menu: (function(){
								var menu = [];
								$.each(ColMedia, function(key, text){
									menu.push({
										text: text,
										menu: addMenuSizes('media', key, true),
										onpostrender: function() {
											enableIfSelected(this, 'td');
											var btn = this;

											var activeCurrentMenu = function(){
												var td = getCurrentTd();
												if(!td) return false;
												var option = td.attr('data-save-media');
												$.each(btn.settings.menu,function(index, elmenu) {
													if($.inArray(elmenu.value, option.split(' ')) > -1){
														btn.active(true);
														return false;
													}else{
														btn.active(false);
													}
												});

											};
											activeCurrentMenu();
											editor.on('NodeChange', function(e) {
												activeCurrentMenu();
											});
										}
									});
								});

								return menu;
							})(),
						},
						{
							text: 'Size',
							menu: addMenuSizes('size'),
						},
						{
							text: 'Insert column before',
							onclick: function() {
								var td = getCurrentTd('td');
								if(!td) return false;
								var cloneTd = td.clone(true);
								td.before(clearCol(cloneTd));

								editor.undoManager.add();
							},
							onpostrender: function() {
								enableIfSelected(this, 'td');
							}
						},
						{
							text: 'Insert column after',
							onclick: function() {
								var td = getCurrentTd('td');
								if(!td) return false;
								var cloneTd = td.clone(true);
								td.after(clearCol(cloneTd));

								editor.undoManager.add();
							},
							onpostrender: function() {
								enableIfSelected(this, 'td');
							}
						},
						{
							text: 'Remove',
							onclick: function() {
								var td = getCurrentTd();
								if(!td) return false;
								var table = td.closest('table');
								td.remove();
								if(!$('td', table).length)
									table.remove();

								editor.undoManager.add();
							},
							onpostrender: function() {
								enableIfSelected(this, 'td');
							}
						},
					],
				},
				{
					text: 'Reset width and height',
					onclick: function() {
						var table = getCurrentTd('table');
						if(!table) return false;
						table.attr('data-mce-style', table.removeAttr('width').css('width', '100%').removeAttr('height').css('height', '').attr('style'));
						table.find('> tbody > tr').removeAttr('height').css('height', '');
						table.find('> tbody > tr > td').removeAttr('height').css('height', '');

						editor.undoManager.add();
					},
					onpostrender: function() {
						enableIfSelected(this, 'td');
					}
				},
			]
		}
	});

	editor.on( 'BeforeSetContent', function( event ) {
		if ( event.content ) {
			if ( event.content.indexOf( '[row' ) !== -1 ) {
				event.content = event.content.replace( /(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?\s{0,}\[row([^\]]+)?\]\s{0,}(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?/gim, function( match, args ) {
					args = args || '';
					var tag_args = $('<div '+args+'>');
					var attrClass = tag_args.attr('class') || '';
					var attrWidth = getWidth(tag_args);
					var attrHeight = getHeight(tag_args);
					var styleWidth = attrWidth.indexOf('%') < 0? 'width:'+attrWidth+'px;' : 'width: '+attrWidth+';';
					var styleHeight = attrHeight? 'height:'+attrHeight+'px;' : '';
					return '<table data-wp-b-row="row" data-wp-b-row-args="" width="'+attrWidth+'" style="'+styleWidth+''+styleHeight+'" ' +
						'class="wp-columns-row wp-b-row-tag mce-wp-b-row" data-mce-resize="true" data-mce-placeholder="1"><tbody class="row '+attrClass+'"><tr>';
				});
				event.content = event.content.replace(/(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?\s{0,}\[\/row\]\s{0,}(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?/gim, function( match ) {
					return '</tr></tbody></table>';
				});
			}
			if ( event.content.indexOf( '[col' ) !== -1 ) {
				event.content = event.content.replace(/(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?\s{0,}\[col([^\]]+)?class="w-100"([^\]]+)?\](\s+)?\[\/col\]\s{0,}(?:<\/p>|<br[^>]{0,}>)?/gim, '</tr><tr>');
				event.content = event.content.replace(/(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?\s{0,}\[col([^\]]+)?\]\s{0,}(?:<\/p>|<br[^>]{0,}>)?/gim, function( match, args ) {
					args = args || '';
					var tag_args = $('<div '+args+'>');
					var attrSize = tag_args.attr('size') || '';
					var attrMedia = tag_args.attr('media') || '';
					var attrClass = tag_args.attr('class') || '';
					var attrHeight = tag_args.attr('height') || '';
					return '<td data-wp-b-col="col" data-save-size="'+attrSize+'" data-save-media="'+attrMedia+'" data-save-class="'+attrClass+'"' +
						'class="wp-columns-col wp-b-col-tag mce-wp-b-col col '+attrSize+' '+attrMedia+' '+attrClass+'" height="'+attrHeight+'" data-mce-resize="true" data-mce-placeholder="1"><p>';
				});
				event.content = event.content.replace(/(?:<p[^>]{0,}>|<br[^>]{0,}>)?\s{0,}\[\/col\]\s{0,}(?:<p[^>]{0,}>|<\/p>|<br[^>]{0,}>)?/gim, function( match, args, shortcode_text ) {
					return '</p></td>';
				});
			}
		}
	});

	editor.on( 'PostProcess', function( event ) {

		if ( event.get ) {
			var EvenContent = $('<div id="my-wrap-delete">'+event.content+'</div>');
			$($('table[data-wp-b-row="row"]', EvenContent).get().reverse()).each(function(index, el) {
				var $this = $(this);
				var saveClass = $this.children('tbody').removeClass('row').attr('class') || '';
				var width = getWidth($this);
				var height = getHeight($this);
				var col = '';
				var countTr = $this.find('> tbody > tr').length;
				$this.find('> tbody > tr').each(function(index, el) {
					var $thisTR = $(el);

					$thisTR.find('> td').each(function(index, el) {
						var $thisCol = $(el);
						var saveSizeCol = $thisCol.attr('data-save-size') || '';
						var saveMediaCol = $thisCol.attr('data-save-media') || '';
						var saveClassCol = $thisCol.attr('data-save-class') || '';
						var heightCol = getHeight($thisCol);
						col += '<column:col size="'+saveSizeCol+'" media="'+saveMediaCol+'" class="'+saveClassCol+'" height="'+heightCol+'">'+el.innerHTML+'</column:col>';
					});

					if(countTr && countTr > index+1)
						col += '<column:col class="w-100"></column:col>';
				});

				$this.after('<column:row class="'+saveClass+'" width="'+width+'" height="'+height+'">'+col+'</column:row>');
				// $this.after('<column:row class="'+saveClass+'" width="'+width+'" height="'+height+'">'+col.replace(/<p>|/gmi, "").replace(/<\/p>/gmi, "\n")+'</column:row>');
				$this.remove();
			});
			var EvenContentHtml = EvenContent.html();
			EvenContentHtml = EvenContentHtml.replace(/<column:row([^>]+)?>/gim, '[row$1]');
			EvenContentHtml = EvenContentHtml.replace(/<\/column:row>/gim, '[/row]');
			EvenContentHtml = EvenContentHtml.replace(/<column:col([^>]+)?>/gim, '[col$1]');
			EvenContentHtml = EvenContentHtml.replace(/<\/column:col>/gim, '[/col]');
			event.content = EvenContentHtml;
		}

	});

	editor.on('init', function(){ 
		var EditorWindow = document.getElementById(editor.iframeElement.id).contentWindow;
		var currentSize = function(){
			$(EditorWindow.document).find('.wp-columns-row').attr('currentMQ', $(editor.iframeElement).width())
		}
		currentSize();
		$(EditorWindow).resize(currentSize);
	});

	function noop() {}

	// Expose some functions (back-compat)
	return {
		_showButtons: noop,
		_hideButtons: noop,
		_setEmbed: noop,
		_getEmbed: noop
	};
});

}( window.tinymce ));